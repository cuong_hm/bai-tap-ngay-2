import scala.io.Source

class MyClass {
   var myFriend :Int = 0

//  contructor
  def this(values: Int) = {
    this();
    this.myFriend = values
  }

//  Cac phuong thuc
  def getMyFriend():Int = {
   return this.myFriend
  }

  def addToMyFriend(value: Int): Unit ={
    this.myFriend += value
  }
}

// abstract class
abstract class Bike(a:Int){
  var b:Int = 20
  var c:Int = 25
  def run()
  def performance(): Unit ={
    println("Performance awesome")
  }
}
class Hero(a:Int) extends Bike(a){
//  c = 30
  def run(): Unit ={
    println("Running fine..")
    println("a = "+a)
    println("b = "+b)
    println("c = "+c)
  }
}
object MainObject{
  def main(args: Array[String]): Unit ={
    val h = new Hero(10)
    h.run()
    h.performance()
  }
}

//Trait
trait Cardetails{
  def details(d:String):String
}
class Cardet extends Cardetails{
   def details(source: String):String = {
    Source.fromString(source).mkString
  }
}

trait detcar{
  def readdetails(d:String):String =
    Source.fromString(d).mkString
}
class Car (var cname:String, var cno: Int){
  def details = cname + " " + cno
}

class Alto (cname: String, cno:Int, var color:String) extends Car(cname, cno) with detcar{
  override def details: String = {
//    val det = readdetails(color)
    cname+"\n"+cno+"\n"+"Color:"+color
  }
}
object cartest{
  def main(args: Array[String]): Unit ={
//    val a1 = new Alto("Alto", 34, "Black")
//    println(a1.details)
    println(dracula.hob)
  }
}
class Vam ( name: String){
  def hob = "sucking"
}
val dracula = new Vam("haha")
