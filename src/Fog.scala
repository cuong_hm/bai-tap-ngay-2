trait Philosophical {
  def philosophiez(): Unit ={
    println("I consume memory, therefore I am!")
  }
}
class Fog extends Philosophical{
  override def toString = "Green"
}
object Hello extends App{
  val fog = new Fog
  fog.philosophiez()
}