 object Day6 {
    class List[A](items: A*) {
      val item: Option[A] = items.headOption
      val next: Option[List[A]] = {
        if (item.isDefined) Some(new List(items.tail: _*))
        else None
      }

      def foreach(f: A => Unit): Unit = {
        for (i <- item; n <- next) {
          f(i)
          n.foreach(f)
        }
      }

      def apply(index: Int): Option[A] = {
        if (index < 1) item
        else next  flatMap (_.apply(index - 1))
      }
    }

    def main(args: Array[String]): Unit = {
      val x = new List(1,2,3,4,5)
      x foreach println
    }

  }

