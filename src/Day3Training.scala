import com.sun.xml.internal.ws.api.streaming.XMLStreamWriterFactory.Default

import java.awt.print.Book
import java.net.MalformedURLException

object Day3Training extends App {
////bai 2
//   def fives(cur: Int, max: Int): Unit = {
//     if (cur <= max) {
//           println(cur)
//           fives(cur + 5, max)
//        }
//   }
//   fives(5,50)

//  bai 11case class Book(title: String, authors: String*)
  ////val books: List[Book] = {
  ////  List(
  ////    Book(
  ////      "Structure and Interpretation of Computer Programs",
  ////      "Abelson, Harold", "Sussman, Gerald J."
  ////    ), Book(
  ////      "Principles of Compiler Design",
  ////      "Aho, Alfred", "Ullman, Jeffrey"
  ////    ),
  ////    Book(
  ////      "Programming in Modula-2",
  ////      "Wirth, Niklaus"
  ////    ),
  ////    Book(
  ////      "Elements of ML Programming",
  ////      "Ullman, Jeffrey"
  ////    ),
  ////    Book(
  ////      "The Java Language Specification", "Gosling, James",
  ////      "Joy, Bill", "Steele, Guy", "Bracha, Gilad"
  ////    )
  ////  )
  ////}
  ////  for (b <- books; a <- b.authors
  ////       if a startsWith("Gosling"))
  ////  yield println(b.title)
  ////
  ////  for (b <- books if (b.title indexOf "Program") >= 0)
  ////    yield println("List 2: " + b.title)
//
//bai 11
  def conditional(x:Int,default: String, p: Int => Boolean, f:Int => String):String =
    if (p(x)) f(x) else default

  def p(x: Int): Boolean = {
    (x % 5 == 0 || x % 3 == 0)
  }

  def f(x: Int): String = {
    var a = ""
    if (x % 5 == 0) a += "type"
    if (x % 3 == 0) a += "safe"
    a
  }
  for (i <- 1 to 100) {
    println(conditional(i, "" + i, p, f))
  }
}
