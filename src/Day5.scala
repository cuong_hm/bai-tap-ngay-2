import java.io.{File, FileWriter, PrintWriter}
import scala.io.Source
import scala.util.matching.Regex

  object Day5 {

    def replaceInFile(search: String, replace: String, file: File): Unit = {
      val text = read(file)
      createBackupFile(text, file)

      val updated = text.replaceAll(search, replace)
      write(updated, file)
    }

    def createBackupFile(s: String, file: File): Unit = {
      var dir = new File(file.getAbsoluteFile.getParent)
      var backupFile = new File(dir, s"${file.getName}.bak")
      while (backupFile.exists()) {
        backupFile = new File(dir, s"${file.getName}_${System.currentTimeMillis()}.bak")
      }
      write(s, backupFile)
    }

    def replaceInFileNames(search: String, replace: String, files: List[String]): Unit = {
      val validFile: List[File] = files map (new File(_)) filter (_.exists())

      validFile foreach { f => replaceFile(search, replace, f) }
    }

    def replaceFile(search: String, replace: String, file: File): Unit = {

      val text = read(file)
      createBackupFile(text, file)
    }

    def read(file: File) = io.Source.fromFile(file).getLines().mkString("\n")

    def write(s: String, file: File): Unit = {
      val writer = new PrintWriter(file)
      writer.write(s)
      writer.close()
    }


    def main(args: Array[String]): Unit = {
      args.toList match {
        case search :: replace :: files if files.nonEmpty => replaceInFileNames(search, replace, files)
        case _ =>
          println("Usage: MultiReplacer <search pattern> <replacement text> file1[file2...]")
      }
    }
  }


object GFT extends App{
  val fileName = "test.txt"
  val fileSource = Source.fromFile(fileName)
  for (line <- fileSource.getLines()){
    println(line)
  }
  fileSource.close()

  def replaceRegex(patter: Regex, line:String,res:String): String ={
    patter.replaceAllIn(line,res)
  }

  def writerFile1(nameFile:String): Unit = {
    val write = new FileWriter(nameFile)
    write.close()
  }
}