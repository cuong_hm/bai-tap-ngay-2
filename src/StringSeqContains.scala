case class Address(city:String, state:String)
case class Student(name: String, address: Seq[Address])

object City{
  def unapply(s:Student): Option[Seq[String]]= {
    Some(
      for (c <- s.address)
        yield c.state
    )
  }
}

class StringSeqContains (value: String){
  def unapply(in: Seq[String]):Boolean = {
    in contains value
  }
}

/// 098-1234-7658

object PatternMatch {

  def f(x: Student) = {
    ???
  }
  def main(args: Array[String]): Unit ={
    val stud = List(
      Student("Harris", List(Address("LosAngeles", "California"))),
      Student("Reena", List(Address("Houston", "Texas"))),
      Student("Rob", List(Address("Dallas", "Texas"))),
      Student("Chris", List(Address("Jacksonville", "Floria")))
    )
    val haha = new StringSeqContains("Texas")
    val students = stud collect{
//      case student if student.address.exists(_.state == "Texas") => student.name
      case student @ City(haha()) => student.name
//      case student: Student => ???
//      case Student(_, _) => ???
//      case student @ Student(name, address) =>
//        println(address)
//        f(Student(name, address))
//        f(student)
    }
    println(students)
  }
}